const path = require('path');
const webpack = require('webpack');

const PATHS = {
  client: path.join(__dirname, 'client'),
  build: path.join(__dirname, 'build')
};

const appEntry = [
  'webpack-dev-server/client?http://localhost:8080/',
  'webpack/hot/only-dev-server',
  PATHS.client
];

const devtool = 'eval';
const plugins = [
  new webpack.NoErrorsPlugin(),
  new webpack.HotModuleReplacementPlugin()
];

module.exports = {
  entry: {
    app: appEntry
  },
  output: {
    path: PATHS.build,
    publicPath: '/',
    filename: 'bundle.js'
  },
  devtool,
  module: {
    loaders: [
      {
        test: /\.less$/,
        loaders: [
          'style',
          'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!autoprefixer!less'
        ]
      },
      {
        test: /\.jpg$/,
        loader: 'file-loader'
      },
      {
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          plugins: ['./server/utils/babelRelayPlugin'].map(require.resolve),
        }
      }
    ]
  },
  plugins
};
