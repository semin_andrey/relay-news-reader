import fs from 'fs';
import path from 'path';
import { default as relayPlugin } from 'babel-relay-plugin';

const jsonFile = path.join(__dirname, '../data/schema.json');

fs.access(jsonFile, fs.F_OK, (err) => {
  // eslint-disable-next-line global-require
  const data = require(jsonFile).data;
  if (!err) module.exports = relayPlugin(data);
});
