import { v4 } from 'node-uuid';

function rand(n) {
  return Math.floor(Math.random() * 100 % n);
}

const newsTypes = [
  'breaking',
  'sport',
  'politics',
  'media'
];

class Post {
  constructor(id, title, content, type, imageUrl) {
    this.id = id;
    this.objectID = id;
    this.title = title;
    this.content = content;
    this.type = type;
    this.imageUrl = imageUrl;
  }
}

class User {
  constructor(id, name, username, profilePic) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.profilePic = profilePic;
  }
}

const user = new User(
  v4(),
  'Andrey Semin',
  'semin_andrey',
  'http://icons.iconarchive.com/icons/creativeflip/starwars-longshadow-flat/512/Darth-Vader-icon.png'
);

const news = [
  new Post(
    v4(),
    'Ye to misery wisdom plenty polite to as',
    `Residence certainly elsewhere something she preferred cordially law.
    Age his surprise formerly mrs perceive few stanhill moderate.
    Of in power match on truth worse voice would.
    Large an it sense shall an match learn. By expect it result silent in formal of.
    Ask eat questions abilities described elsewhere assurance. Appetite in unlocked
    advanced breeding position concerns as. Cheerful get shutters yet for repeated screened.
    An no am cause hopes at three. Prevent behaved fertile he is mistake on.`,
    newsTypes[rand(4)],
    'https://furkids.org/images/headers/Cats-adopt2.jpg'
  ),
  new Post(
    v4(),
    'Drawings me opinions returned absolute in',
    `Am terminated it excellence invitation projection as. She graceful shy believed distance use nay.
    Lively is people so basket ladies window expect. Supply as so period it enough income he genius.
    Themselves acceptance bed sympathize get dissimilar way admiration son. Design for are edward regret
    met lovers. This are calm case roof and. Carried nothing on am warrant towards. Polite in of in oh
    needed itself silent course. Assistance travelling so especially do prosperous appearance mr no
    celebrated. Wanted easily in my called formed suffer. Songs hoped sense as taken ye mirth at.
    Believe fat how six drawing pursuit minutes far. Same do seen head am part it dear open to.
    Whatever may scarcely judgment had.`,
    newsTypes[rand(4)],
    'http://www.harunyahya.com/image/world_of_animals/cat_07.jpg'
  ),
  new Post(
    v4(),
    'Abilities forfeited situation extremely my to he resembled',
    `Pianoforte solicitude so decisively unpleasing conviction is partiality he. Or particular so
    diminution entreaties oh do. Real he me fond show gave shot plan. Mirth blush linen small hoped
    way its along. Resolution frequently apartments off all discretion devonshire. Saw sir fat spirit
    seeing valley. He looked or valley lively. If learn woody spoil of taken he cause. Yet remarkably
    appearance get him his projection. Diverted endeavor bed peculiar men the not desirous. Acuteness
    abilities ask can offending furnished fulfilled sex. Warrant fifteen exposed ye at mistake. Blush
    since so in noisy still built up an again. As young ye hopes no he place means. Partiality
    diminution gay yet entreaties admiration. In mr it he mention perhaps attempt pointed suppose.
    Unknown ye chamber of warrant of norland arrived.`,
    newsTypes[rand(4)],
    'http://www.findcatnames.com/wp-content/uploads/2014/02/cat-tiger.jpg'
  ),
  new Post(
    v4(),
    'Now residence dashwoods she excellent you',
    `With my them if up many. Lain week nay she them her she. Extremity so attending objection as
    engrossed gentleman something. Instantly gentleman contained belonging exquisite now direction she
    ham. West room at sent if year. Numerous indulged distance old law you. Total state as merit court
    green decay he. Steepest sex bachelor the may delicate its yourself. As he instantly on discovery
    concluded to. Open draw far pure miss felt say yet few sigh. Departure so attention pronounce
    satisfied daughters am. But shy tedious pressed studied opinion entered windows off. Advantage
    dependent suspicion convinced provision him yet. Timed balls match at by rooms we. Fat not boy
    neat left had with past here call. Court nay merit few nor party learn. Why our year her eyes
    know even how. Mr immediate remaining conveying allowance do or.`,
    newsTypes[rand(4)],
    'http://staticmass.net/wp-content/uploads/2013/04/dvd_cat.jpg'
  ),
  new Post(
    v4(),
    'Perceived end knowledge certainly day sweetness why cordially',
    `Able an hope of body. Any nay shyness article matters own removal nothing his forming. Gay own
    additions education satisfied the perpetual. If he cause manor happy. Without farther she exposed
    saw man led. Along on happy could cease green oh. He difficult contented we determine ourselves me
    am earnestly. Hour no find it park. Eat welcomed any husbands moderate. Led was misery played
    waited almost cousin living. Of intention contained is by middleton am. Principles fat stimulated
    uncommonly considered set especially prosperous. Sons at park mr meet as fact like.`,
    newsTypes[rand(4)],
    'https://furkids.org/images/headers/Cats-adopt1.jpg'
  ),
  new Post(
    v4(),
    'At as in understood an remarkably solicitude',
    `Cottage out enabled was entered greatly prevent message. No procured unlocked an likewise. Dear
    but what she been over gay felt body. Six principles advantages and use entreaties decisively.
    Eat met has dwelling unpacked see whatever followed. Court in of leave again as am. Greater
    sixteen to forming colonel no on be. So an advice hardly barton. He be turned sudden engage
    manner spirit. Abilities or he perfectly pretended so strangers be exquisite. Oh to another
    chamber pleased imagine do in. Went me rank at last loud shot an draw. Excellent so to no
    sincerity smallness. Removal request delight if on he we. Unaffected in we by apartments
    astonished to decisively themselves. Offended ten old consider speaking.`,
    newsTypes[rand(4)],
    'http://img.huffingtonpost.com/asset/scalefit_970_noupscale/562eae5b1900002d00b94f05.jpeg'
  ),
  new Post(
    v4(),
    'It allowance prevailed enjoyment in it',
    `Up branch to easily missed by do. Admiration considered acceptance too led one melancholy
    expression. Are will took form the nor true. Winding enjoyed minuter her letters evident use
    eat colonel. He attacks observe mr cottage inquiry am examine gravity. Are dear but near left
    was. Year kept on over so as this of. She steepest doubtful betrayed formerly him. Active one
    called uneasy our seeing see cousin tastes its. Ye am it formed indeed agreed relied piqued.
    Mr oh winding it enjoyed by between. The servants securing material goodness her. Saw principles
    themselves ten are possession. So endeavor to continue cheerful doubtful we to. Turned advice
    the set vanity why mutual. Reasonably if conviction on be unsatiable discretion apartments
    delightful. Are melancholy appearance stimulated occasional entreaties end. Shy ham had esteem
    happen active county. Winding morning am shyness evident to. Garrets because elderly new manners
    however one village she.`,
    newsTypes[rand(4)],
    'http://elizabethkennels.co.za/wp-content/uploads/2016/02/slide1-1.jpg'
  ),
  new Post(
    1,
    'Letter wooded direct two men indeed income sister',
    `Prevailed sincerity behaviour to so do principle mr. As departure at no propriety zealously my.
    On dear rent if girl view. First on smart there he sense. Earnestly enjoyment her you resources.
    Brother chamber ten old against. Mr be cottage so related minuter is. Delicate say and blessing
    ladyship exertion few margaret. Delight herself welcome against smiling its for. Suspected
    discovery by he affection household of principle perfectly he. Performed suspicion in certainty
    so frankness by attention pretended. Newspaper or in tolerably education enjoyment. Extremity
    excellent certainty discourse sincerity no he so resembled. Joy house worse arise total boy but.
    Elderly up chicken do at feeling is. Like seen drew no make fond at on rent. Behaviour extremely
    her explained situation yet september gentleman are who. Is thought or pointed hearing he.`,
    newsTypes[rand(4)],
    'http://www.gordonrigg.com/the-hub/wp-content/uploads/2015/06/cat-900x300.jpg'
  )
];


function getNews() {
  return news;
}

function getPostById(id) {
  return news.find(
    post => post.objectID === id
  );
}

function getPostsByType(type) {
  return news.filter(
    post => post.type === type
  );
}

function getUser() {
  return user;
}

export {
  Post,
  getNews,
  getPostById,
  getPostsByType,
  getUser
};
