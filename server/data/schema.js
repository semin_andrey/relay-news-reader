
import {
  GraphQLString,
  GraphQLObjectType,
  GraphQLSchema
} from 'graphql';

import {
  connectionDefinitions,
  globalIdField,
  connectionArgs,
  connectionFromArray
} from 'graphql-relay';

import {
  getNews,
  getPostById,
  getPostsByType,
  getUser
} from './database';

const PostType = new GraphQLObjectType({
  name: 'Post',
  description: 'Post',
  fields: () => ({
    id: globalIdField('Post'),
    objectID: {
      type: GraphQLString,
      description: 'Doubled id of the Post. So we could get it by ID',
    },
    imageUrl: {
      type: GraphQLString,
      description: 'Url of the Post image.',
    },
    title: {
      type: GraphQLString,
      description: 'Title of the Post.',
    },
    content: {
      type: GraphQLString,
      description: 'Content of the Post.',
    },
    type: {
      type: GraphQLString,
      description: 'Type of the Post.',
    }
  })
});

const { connectionType: newsConnection } =
  connectionDefinitions({ name: 'Post', nodeType: PostType });

const UserType = new GraphQLObjectType({
  name: 'User',
  description: 'User',
  fields: () => ({
    id: globalIdField('User'),
    news: {
      type: newsConnection,
      description: 'News from feed',
      args: {
        type: {
          name: 'type',
          type: GraphQLString
        },
        ...connectionArgs
      },
      resolve: (_, args) => {
        if (args.type) {
          return connectionFromArray(getPostsByType(args.type), args);
        }
        return connectionFromArray(getNews(), args);
      }
    },
    post: {
      type: PostType,
      description: 'One post by id',
      args: {
        id: {
          name: 'id',
          type: GraphQLString
        }
      },
      resolve: (root, { id }) => getPostById(id)
    },
    name: {
      type: GraphQLString,
      description: 'Name of the user'
    },
    username: {
      type: GraphQLString,
      description: 'Username of the user'
    },
    profilePic: {
      type: GraphQLString,
      description: 'Avatar of the user'
    }
  }),
});

const queryType = new GraphQLObjectType({
  name: 'RootQuery',
  fields: {
    viewer: {
      type: UserType,
      resolve: () => getUser()
    }
  }
});

const schema = new GraphQLSchema({
  query: queryType
});

export default schema;
