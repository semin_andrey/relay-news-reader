import path from 'path';
import webpack from 'webpack';
import express from 'express';
import graphQLHTTP from 'express-graphql';
import WebpackDevServer from 'webpack-dev-server';
import chalk from 'chalk';
import webpackConfig from '../webpack.config';
import schema from './data/schema';

const config = {
  port: 8080,
  graphql: {
    port: 3000
  }
};

const graphql = express();
graphql.use('/', graphQLHTTP({
  schema,
  graphiql: true,
  pretty: true,
}));

graphql.listen(config.graphql.port, () =>
  // eslint-disable-next-line no-console
  console.log(chalk.green(`GraphQL is listening on port ${config.graphql.port}`)
));

const relayServer = new WebpackDevServer(webpack(webpackConfig), {
  contentBase: webpackConfig.output.path,
  proxy: {
    '/graphql': `http://localhost:${config.graphql.port}`
  },
  stats: {
    colors: true
  },
  hot: true,
  historyApiFallback: true,
  inline: true,
  progress: true
});

relayServer.use('/', express.static(path.join(__dirname, '../build')));
relayServer.listen(config.port, () =>
  // eslint-disable-next-line no-console
  console.log(chalk.green(`Relay is listening on port ${config.port}`)
));
