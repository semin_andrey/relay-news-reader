import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import injectTapEventPlugin from 'react-tap-event-plugin';

import store from './store';
import Router from './router';

injectTapEventPlugin();

const history = syncHistoryWithStore(browserHistory, store);

render(
  <Provider store={store}>
    <Router history={history} />
  </Provider>,
  document.getElementById('app')
);

// makes hot realod work
module.hot.accept();
