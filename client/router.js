import React, { PropTypes } from 'react';
import {
  IndexRoute,
  Route,
  Redirect,
  Router,
  applyRouterMiddleware
} from 'react-router';
import { Store } from 'react-relay';
import useRelay from 'react-router-relay';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import ViewerQuery from './queries/ViewerQuery';
import AppContainer from './components/App/AppContainer';
import NewsContainer from './components/NewsList/NewsContainer';
import FullPostContainer from './components/FullPost/FullPostContainer';
import NewsTypeContainer from './components/NewsList/NewsTypeContainer';


const propTypes = {
  history: PropTypes.object
};

class RouterWrapper extends React.Component {
  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <Router
          history={this.props.history}
          render={applyRouterMiddleware(useRelay)}
          environment={Store}
        >
          <Route path="/" component={AppContainer} queries={ViewerQuery}>
            <IndexRoute component={NewsContainer} queries={ViewerQuery} />
            <Route
              path="/post/:id"
              component={FullPostContainer}
              queries={ViewerQuery}
            />
            <Route
              path="type/:type"
              component={NewsTypeContainer}
              queries={ViewerQuery}
            />
            <Redirect from="*" to="/" />
          </Route>
        </Router>
      </MuiThemeProvider>
    );
  }
}

RouterWrapper.propTypes = propTypes;

export default RouterWrapper;
