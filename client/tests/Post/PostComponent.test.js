import { describe, it } from 'mocha';
import React from 'react';
import { mount, render } from 'enzyme';
import { assert } from 'chai';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Post from './../../components/Post/PostComponent';
import RaisedButton from 'material-ui/RaisedButton';

describe('PostComponent', () => {
  const muiTheme = getMuiTheme();
  const renderWithContext = (func, node) => func(
    <MuiThemeProvider muiTheme={muiTheme}>
      {node}
    </MuiThemeProvider>,
    { context: { muiTheme } }
  );

  const post = {
    title: 'Post title',
    content: 'Some loooooong content',
    type: 'Some type',
    imageUrl: 'http://i43.tinypic.com/fjfghz.jpg',
    action: (
      <RaisedButton label="action" />
    )
  };

  it('should render an Post component', () => {
    const wrapper = renderWithContext(mount,
      <Post
        title={post.title}
        content={post.content}
        type={post.type}
        imageUrl={post.imageUrl}
        action={post.action}
      />
    );
    assert.equal(wrapper.find(Post).length, 1, 'Post component should be rendered');
  });

  it('should render data correct', () => {
    const wrapper = renderWithContext(render,
      <Post
        title={post.title}
        content={post.content}
        type={post.type}
        imageUrl={post.imageUrl}
        action={post.action}
      />
    );
    assert.equal(wrapper.find('span')[0].children[0].data, post.title, 'Post title should be rendered');
    assert.equal(wrapper.find('img')[0].attribs.src, post.imageUrl, 'Post image should have src from the props');
    assert.equal(wrapper.find('p')[0].children[0].data, post.content, 'Post content should be rendered');
    assert.equal(wrapper.find('span')[2].children[0].data, post.type, 'Post type should be rendered');
  });
});
