import { describe, it } from 'mocha';
import React from 'react';
import { mount, render } from 'enzyme';
import { assert } from 'chai';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import UserBox from './../../components/UserBox/UserBoxComponent';

describe('UserBoxComponent', () => {
  const muiTheme = getMuiTheme();
  const renderWithContext = (func, node) => func(
    <MuiThemeProvider muiTheme={muiTheme}>
      {node}
    </MuiThemeProvider>,
    { context: { muiTheme } }
  );

  const user = {
    name: 'Anonymous',
    username: 'anonymous',
    image: 'http://i43.tinypic.com/fjfghz.jpg'
  };

  it('should render an UserBox component', () => {
    const wrapper = renderWithContext(mount,
      <UserBox
        name={user.name}
        username={user.username}
        image={user.image}
      />
    );
    assert.equal(wrapper.find(UserBox).length, 1, 'UserBox component should be rendered');
  });

  it('should render an UserBox component', () => {
    const wrapper = renderWithContext(render,
      <UserBox
        name={user.name}
        username={user.username}
        image={user.image}
      />
    );
    assert.equal(wrapper.find('p')[0].children[0].data, user.name, 'User name should be rendered');
    assert.include(wrapper.find('p')[1].children[0].data, user.username, 'User handle should be rendered');
    assert.equal(wrapper.find('img')[0].attribs.src, user.image, 'UserBox image should have src from the props');
  });
});
