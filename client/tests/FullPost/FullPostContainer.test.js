import { describe, it } from 'mocha';
import React from 'react';
import { mount } from 'enzyme';
import { assert } from 'chai';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FullPost from './../../components/FullPost/FullPostComponent';

describe('FullPostComponent', () => {
  const muiTheme = getMuiTheme();
  const renderWithContext = (func, node) => func(
    <MuiThemeProvider muiTheme={muiTheme}>
      {node}
    </MuiThemeProvider>,
    { context: { muiTheme } }
  );

  const viewer = {
    post: {
      title: 'Post title',
      content: 'Some loooooong content',
      type: 'Some type',
      imageUrl: 'http://i43.tinypic.com/fjfghz.jpg'
    }
  };

  it('should render an FullPost component', () => {
    const wrapper = renderWithContext(mount,
      <FullPost
        viewer={viewer}
      />
    );
    assert.equal(wrapper.find(FullPost).length, 1, 'FullPost component should be rendered');
  });
});
