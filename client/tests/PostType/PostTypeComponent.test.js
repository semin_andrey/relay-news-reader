import { describe, it } from 'mocha';
import React from 'react';
import { mount, render } from 'enzyme';
import { assert } from 'chai';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import PostType from './../../components/PostType/PostTypeComponent';

describe('PostTypeComponent', () => {
  const muiTheme = getMuiTheme();
  const renderWithContext = (func, node) => func(
    <MuiThemeProvider muiTheme={muiTheme}>
      {node}
    </MuiThemeProvider>,
    { context: { muiTheme } }
  );

  const types = {
    media: 'media',
    sport: 'sport',
    politics: 'politics',
    breaking: 'breaking',
  };

  const colors = {
    media: '#90a4ae',
    sport: '#81c784',
    politics: '#64b5f6',
    breaking: '#ffb74d',
  };

  it('should render an PostType component', () => {
    const wrapper = renderWithContext(mount,
      <PostType
        text={types.media}
      />
    );
    assert.equal(wrapper.find(PostType).length, 1, 'PostType component should be rendered');
  });

  it('should calculate media color correct', () => {
    const wrapper = renderWithContext(render,
      <PostType
        text={types.media}
      />
    );
    assert.include(wrapper.find('div[type=button]')[0].attribs.style, colors.media, 'Color for media type should be calculated correct');
  });

  it('should calculate sport color correct', () => {
    const wrapper = renderWithContext(render,
      <PostType
        text={types.sport}
      />
    );
    assert.include(wrapper.find('div[type=button]')[0].attribs.style, colors.sport, 'Color for sport type should be calculated correct');
  });

  it('should calculate politics color correct', () => {
    const wrapper = renderWithContext(render,
      <PostType
        text={types.politics}
      />
    );
    assert.include(wrapper.find('div[type=button]')[0].attribs.style, colors.politics, 'Color for politics type should be calculated correct');
  });

  it('should calculate breaking color correct', () => {
    const wrapper = renderWithContext(render,
      <PostType
        text={types.breaking}
      />
    );
    assert.include(wrapper.find('div[type=button]')[0].attribs.style, colors.breaking, 'Color for breaking type should be calculated correct');
  });
});
