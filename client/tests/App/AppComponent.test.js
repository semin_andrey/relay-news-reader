import { describe, before, it } from 'mocha';
import React from 'react';
import { mount } from 'enzyme';
import { assert } from 'chai';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import App from './../../components/App/AppComponent';

describe('AppComponent', () => {
  before(() => {
    injectTapEventPlugin();
  });

  const muiTheme = getMuiTheme();
  const renderWithContext = (func, node) => func(
    <MuiThemeProvider muiTheme={muiTheme}>
      {node}
    </MuiThemeProvider>,
    { context: { muiTheme } }
  );

  const user = {
    name: 'Anonymous',
    username: 'anonymous',
    profilePic: 'http://i43.tinypic.com/fjfghz.jpg'
  };

  it('should render an App component', () => {
    const wrapper = renderWithContext(mount,
      <App viewer={user} />
    );

    assert.equal(wrapper.find(App).length, 1, 'App component should be rendered');
  });
});
