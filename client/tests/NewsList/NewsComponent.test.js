import { describe, it } from 'mocha';
import React from 'react';
import { mount } from 'enzyme';
import { assert } from 'chai';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import NewsList from './../../components/NewsList/NewsComponent';
import Post from './../../components/Post/PostComponent';
import Error from './../../components/Error/ErrorComponent';

describe('NewsListComponent', () => {
  const muiTheme = getMuiTheme();
  const renderWithContext = (func, node) => func(
    <MuiThemeProvider muiTheme={muiTheme}>
      {node}
    </MuiThemeProvider>,
    { context: { muiTheme } }
  );

  const viewer = {
    news: {
      edges: [
        {
          node: {
            objectID: '1',
            title: 'Post title',
            content: 'Some loooooong content',
            type: 'Some type',
            imageUrl: 'http://i43.tinypic.com/fjfghz.jpg'
          }
        },
        {
          node: {
            objectID: '2',
            title: 'Another post title',
            content: 'Some very looooooooooooooooong content',
            type: 'Another type',
            imageUrl: 'http://i39.tinypic.com/dfwpd.jpg'
          }
        }
      ]
    }
  };

  const viewerWithoutNews = {
    news: {
      edges: []
    }
  };

  it('should render an NewsList component', () => {
    const wrapper = renderWithContext(mount,
      <NewsList
        viewer={viewer}
      />
    );
    assert.equal(wrapper.find(NewsList).length, 1, 'NewsList component should be rendered');
  });

  it('should render some Post components', () => {
    const wrapper = renderWithContext(mount,
      <NewsList
        viewer={viewer}
      />
    );
    assert.equal(wrapper.find(Post).length, 2, 'Post components amount should be equal to news array length');
  });

  it('should render Error components if there is no news', () => {
    const wrapper = renderWithContext(mount,
      <NewsList
        viewer={viewerWithoutNews}
      />
    );
    assert.equal(wrapper.find(Error).length, 1, 'Error component should be rendred if there is no news');
  });
});
