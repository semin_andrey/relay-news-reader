import { describe, it } from 'mocha';
import React from 'react';
import { mount, render } from 'enzyme';
import { assert } from 'chai';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Card } from 'material-ui/Card';
import Error from './../../components/Error/ErrorComponent';

describe('ErrorComponent', () => {
  const muiTheme = getMuiTheme();
  const renderWithContext = (func, node) => func(
    <MuiThemeProvider muiTheme={muiTheme}>
      {node}
    </MuiThemeProvider>,
    { context: { muiTheme } }
  );

  const error = {
    title: 'Error',
    description: 'Something went wrong!',
  };

  it('should render an Error component', () => {
    const wrapper = renderWithContext(mount,
      <Error
        title={error.title}
        description={error.description}
      />
    );
    assert.equal(wrapper.find(Error).length, 1, 'Error component should be rendered');
    assert.equal(wrapper.find(Card).length, 1, 'Error component should contain Card component');
  });

  it('should render props correct', () => {
    const wrapper = renderWithContext(render,
      <Error
        title={error.title}
        description={error.description}
      />
    );
    assert.include(wrapper.find('span').text(), error.title, 'Error title should be rendered');
    assert.include(wrapper.find('div').text(), error.description, 'Error description should be rendered');
  });
});
