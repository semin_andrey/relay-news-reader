import Relay from 'react-relay';

export default {
  viewer: (Component, params) => Relay.QL`
    query {
      viewer {
        ${Component.getFragment('viewer', params)}
      }
    }
  `
};
