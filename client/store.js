import { createStore, combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

export default createStore(
  combineReducers(Object.assign({}, { routing: routerReducer }))
);
