import Relay from 'react-relay';
import FullPost from './FullPostComponent';

export default Relay.createContainer(FullPost, {
  initialVariables: {
    id: null
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on User {
        post(id: $id) {
          title
          content
          type
          imageUrl
        }
      }`
  }
});
