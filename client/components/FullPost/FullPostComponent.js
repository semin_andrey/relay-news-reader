import React, { PropTypes } from 'react';
import { browserHistory } from 'react-router';
import FlatButton from 'material-ui/FlatButton';
import Post from './../Post/PostComponent';

const propTypes = {
  viewer: PropTypes.object.isRequired,
};

class FullPost extends React.Component {

  handleBackTap() {
    browserHistory.goBack();
  }

  render() {
    const post = this.props.viewer.post;
    return (
      <Post
        title={post.title}
        content={post.content}
        type={post.type}
        imageUrl={post.imageUrl}
        action={
          <FlatButton
            secondary
            label="Back"
            onTouchTap={this.handleBackTap}
          />
        }
      />
    );
  }
}

FullPost.propTypes = propTypes;

export default FullPost;
