import React, { PropTypes } from 'react';
import Avatar from 'material-ui/Avatar';
import styles from './UserBox.less';

const propTypes = {
  name: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired
};

class UserBox extends React.Component {
  render() {
    return (
      <div className={styles.userBox}>
        <div className={styles.userBoxTextWrapper}>
          <p className={styles.userBoxHeading}>
            {this.props.name}
          </p>
          <p>
            @{this.props.username}
          </p>
        </div>
        <div className={styles.userBoxAvatarWrapper}>
          <Avatar
            className={styles.userBoxAvatar}
            src={this.props.image}
            size={50}
            backgroundColor="white"
          />
        </div>
      </div>
    );
  }
}

UserBox.propTypes = propTypes;

export default UserBox;
