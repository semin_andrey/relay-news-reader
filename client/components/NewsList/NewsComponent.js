import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import Post from './../Post/PostComponent';
import Error from './../Error/ErrorComponent';

const MAX_CONTENT_LENGTH = 500;

const propTypes = {
  viewer: PropTypes.object.isRequired,
  location: PropTypes.object
};

class NewsList extends React.Component {

  ellipseStr(str) {
    return str.length > (MAX_CONTENT_LENGTH - 3)
    ? `${str.substring(0, MAX_CONTENT_LENGTH - 3)}...`
    : str;
  }

  renderContent(edges) {
    return (
      edges.map(edge => {
        const { objectID, title, content, type, imageUrl } = edge.node;
        return (
          <Post
            key={objectID}
            title={title}
            content={this.ellipseStr(content)}
            type={type}
            imageUrl={imageUrl}
            action={
              <Link to={`/post/${objectID}`}>
                <RaisedButton
                  secondary
                  label="Read More"
                />
              </Link>
            }
          />
        );
      })
    );
  }

  renderError() {
    return (
      <Error
        title="Ooops!"
        description="Seems like something went wrong."
      />
    );
  }

  render() {
    const edges = this.props.viewer.news.edges;
    return (
      <div>
        {
          edges.length
          ? this.renderContent(edges)
          : this.renderError()
        }
      </div>
    );
  }
}

NewsList.propTypes = propTypes;

export default NewsList;
