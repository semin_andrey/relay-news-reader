import Relay from 'react-relay';
import NewsList from './NewsComponent';

export default Relay.createContainer(NewsList, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on User {
        news(first: 10) {
          edges {
            node {
              objectID
              title
              type
              content
              imageUrl
            }
          }
        }
      }`
  }
});
