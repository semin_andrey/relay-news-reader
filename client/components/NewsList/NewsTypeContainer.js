import Relay from 'react-relay';
import NewsList from './NewsComponent';

export default Relay.createContainer(NewsList, {
  initialVariables: {
    type: null
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on User {
        news(type: $type first: 10) {
          edges {
            node {
              objectID
              title
              type
              content
              imageUrl
            }
          }
        }
      }`
  }
});
