import React, { PropTypes } from 'react';
import AppBar from 'material-ui/AppBar';
import { Link } from 'react-router';
import UserBox from './../UserBox/UserBoxComponent';
import styles from './App.less';

const propTypes = {
  viewer: PropTypes.object.isRequired,
  children: PropTypes.object
};

class App extends React.Component {

  render() {
    return (
      <div className={styles.root}>
        <AppBar
          title={
            <Link to="/" className={styles.rootLink}>
              Relay News Reader
            </Link>
            }
          iconElementLeft={<span />}
        >
          <UserBox
            name={this.props.viewer.name}
            username={this.props.viewer.username}
            image={this.props.viewer.profilePic}
          />
        </AppBar>
        <div className={styles.contentWrapper}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

App.propTypes = propTypes;

export default App;
