import React, { PropTypes } from 'react';
import { Card, CardMedia, CardActions, CardTitle, CardText } from 'material-ui/Card';
import { Link } from 'react-router';
import PostType from './../PostType/PostTypeComponent';
import styles from './Post.less';

const propTypes = {
  title: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  action: PropTypes.object.isRequired
};

class Post extends React.Component {
  render() {
    return (
      <Card className={styles.card}>
        <CardMedia
          overlay={
            <CardTitle
              title={this.props.title}
            />
          }
        >
          <img src={this.props.imageUrl} alt={this.props.title} />
        </CardMedia>
        <CardText>
          <p className={styles.content}>
            {this.props.content}
          </p>
        </CardText>
        <CardActions className={styles.actionsWrapper}>
          <Link to={`/type/${this.props.type}`}>
            <PostType
              text={this.props.type}
            />
          </Link>
          {this.props.action}
        </CardActions>
      </Card>
    );
  }
}

Post.propTypes = propTypes;

export default Post;
