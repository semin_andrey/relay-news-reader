import React, { PropTypes } from 'react';
import { Card, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import styles from './Error.less';
import image from './../../assets/images/error.jpg';

const propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};

class Error extends React.Component {
  render() {
    return (
      <Card className={styles.container}>
        <CardMedia
          overlay={
            <CardTitle
              title={this.props.title}
            />
          }
        >
          <img src={image} alt="Sad cat" />
        </CardMedia>
        <CardText className={styles.content}>
          {this.props.description}
        </CardText>
      </Card>
    );
  }
}

Error.propTypes = propTypes;

export default Error;
