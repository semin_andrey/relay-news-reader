import React, { PropTypes } from 'react';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import {
  orange300,
  orange900,
  green300,
  green900,
  blue300,
  blue900,
  blueGrey300,
  blueGrey900
} from 'material-ui/styles/colors';
import styles from './PostType.less';

const propTypes = {
  text: PropTypes.string.isRequired
};

class PostType extends React.Component {

  getAvatarLabel(text) {
    return text.substring(0, 2).toUpperCase();
  }

  getColors(type) {
    switch (type) {
      case 'breaking':
        return {
          primary: orange300,
          background: orange900
        };
      case 'sport':
        return {
          primary: green300,
          background: green900
        };
      case 'politics':
        return {
          primary: blue300,
          background: blue900
        };
      case 'media':
        return {
          primary: blueGrey300,
          background: blueGrey900
        };
      default:
        return {
          primary: blueGrey300,
          background: blueGrey900
        };
    }
  }

  render() {
    let { text } = this.props;
    const colors = this.getColors(text);
    return (
      <Chip
        backgroundColor={colors.primary}
        className={styles.chip}
      >
        <Avatar
          size={32}
          color={colors.primary}
          backgroundColor={colors.background}
        >
          {this.getAvatarLabel(text)}
        </Avatar>
        {text}
      </Chip>
    );
  }
}

PostType.propTypes = propTypes;

export default PostType;
